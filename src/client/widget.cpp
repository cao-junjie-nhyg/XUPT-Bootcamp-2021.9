#include "widget.h"
#include "./ui_widget.h"
#include <QPixmap>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    stop = false;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::sendRequest()
{
    QNetworkRequest request;
    //设置请求的URL
    request.setUrl(QUrl(ui->lineEdit->text()));
    response = client.get(request);
    connect(response, SIGNAL(finished()), this, SLOT(responseReceived()));
}

void Widget::responseReceived()
{
    QPixmap frame;
    frame.loadFromData(response->readAll());
    response->deleteLater();
    ui->labelFrame->setPixmap(frame);
    if (!stop)
    {
        //请求下一帧图像
        sendRequest();
    }
}

void Widget::on_pushButton_clicked(bool checked)
{
    if (checked == true)
    {
        //开始播放
        sendRequest();
        stop = false;
        ui->pushButton->setText("Stop");
    }
    else
    {
        //停止播放
        stop = true;
        ui->pushButton->setText("Start");
    }
}
