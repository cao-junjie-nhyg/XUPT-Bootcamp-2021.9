#include <stdio.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <signal.h>
int main()
{
    //创建监听套接字
    int listenfd = socket(PF_INET,SOCK_STREAM,0);

    //本程序的IP地址和端口号
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; //IPV4地址（地址类型）
    addr.sin_port = htons(7);//端口号（网络字节字）
    addr.sin_addr.s_addr = htonl(INADDR_ANY);//绑定本机IP地址
    //addr.sin_addr.s_addr = inet_addr("192.168.1.127");//指定IP地址进行监听

    //将IP地址与监听套接字绑定
    int error = bind(listenfd,(struct sockaddr*)&addr,sizeof(addr));
    if(error)
    {  //打印错误原因
        perror("bind");
        return 1; //bind函数出错，则返回1
    }

    error = listen(listenfd,3);
    if(error)
    {  //打印错误原因
        perror("listen");
        return 2; //listen函数出错，则返回2
    }

    //忽略SIGPIPE信号，防止发送数据时被OS终结
    signal(SIGPIPE,SIG_IGN);


    while(1)
  {
    struct sockaddr_in client_addr;
    socklen_t addrlen = sizeof(client_addr);
    //等待客户端连接
    int connectfd = accept(listenfd,(struct sockaddr*)&client_addr,&addrlen);//阻塞函数
 //  if(connectfd = -1)
  // {
  //     perror("accept");
  //     continue;
 //  }
    printf("client is connected,ip:%s,port:%d\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));


    //使用标准IO函数与客户端通信，需要将连接套接字与标准IO文件流绑定
    FILE* fp = fdopen (connectfd,"r+");
  // if(!fp)
  // {
   //    perror("fdopen");
   //    continue;
  // }
    char line[80];
    //从客户端读取一行输入
    while(1)
   {
        while( fgets(line,sizeof(line),fp))
        {
          if (line[0]=='\r')
         {
            break;
         }
        }
        //判断客户端是否断开连接
   //     if(ferror(fp))
   //     {
   //         printf("Client disconnected\n");
    //        break;
   //     }
        //发送响应报文
        fprintf(fp,"HTTP/1.1 200 OK\r\n");
        fprintf(fp,"Content-Type:text/plain;charset=utf-8\r\n");
        fprintf(fp,"Content-Length:12\r\n");
        fprintf(fp,"\r\n");
        fprintf(fp,"Hello world!");


   }

    fclose(fp);
    }
    return 0;
}
