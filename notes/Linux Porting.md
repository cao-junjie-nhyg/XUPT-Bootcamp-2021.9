## 驱动网卡

设备树文档：linux-source-5.10\Documentation\devicetree\bindings

![image-20210923100214158](Linux%20Porting.assets/image-20210923100214158.png)

DM9000的16根数据线连接到SROM控制器，片选线连接到SROM控制器的CS1，因此当CPU访问0x05000000到0x06000000的地址时，CS1片选线会选中，并且地址线上的信号会发送到SROM控制器的地址线上。

![image-20210923100401254](Linux%20Porting.assets/image-20210923100401254.png)

由于ADDR2地址线连接到DM9000网卡的CMD引脚，所以当CPU访问的地址中第3个比特位为0时，CMD引脚为低电平，DM9000网卡芯片会将数据线上的信号解释为地址；当CPU访问的地址中第3个比特位为1时，CMD引脚为高电平，DM9000网卡芯片会将数据线上的信号解释为数据。

![image-20210923100853759](Linux%20Porting.assets/image-20210923100853759.png)

因此，设备树中的地址寄存器和数据寄存器可以配置为0x05000000到0x06000000地址空间中的值，其中当地址的第三个比特位为0时，作为地址寄存器访问，当地址的第三个比特位为1时，作为数据寄存器访问。

网卡的中断线连接到XEINT6（GPX0_6），因此中断线连接的中断控制器为GPX0，中断线编号为6，中断触发方式为高电平触发。

![image-20210923102221151](Linux%20Porting.assets/image-20210923102221151.png)

![image-20210923102258751](Linux%20Porting.assets/image-20210923102258751.png)

网卡的设备树节点：

```
	ethernet@5000000 {
		compatible = "davicom,dm9000";
		reg = <0x05000000 0x2 0x05000004 0x2>;
		interrupt-parent = <&gpx0>;
		interrupts = <6 4>;
		local-mac-address = [00 00 de ad be ef];
		davicom,no-eeprom;
	};
```

在内核中添加网卡驱动

1. 添加依赖的软件包

```bash
sudo apt install libncurses-dev
```

2. 配置内核

```
cd linux-source-5.10
make menuconfig ARCH=arm
```

选中在以太网卡列表中选中DM9000

![image-20210923103120704](Linux%20Porting.assets/image-20210923103120704.png)

保存退出

![image-20210923103326119](Linux%20Porting.assets/image-20210923103326119.png)

3. 重新编译内核和设备树
4. 重启开发板，加载新的内核和设备树
5. 配置开发板IP地址

```bash
ip link set eth0 up
ip addr add 172.16.1.2/16 dev eth0
```





## 驱动USB摄像头

### 驱动USB集线器

![image-20210923112657171](Linux%20Porting.assets/image-20210923112657171.png)

查看USB集线器设备树文档：linux-source-5.10\Documentation\devicetree\bindings\usb\usb3503.txt

修改对应的GPIO引脚和工作模式。

```
	usb3503 {
		compatible = "smsc,usb3503a";
		connect-gpios = <&gpm3 3 GPIO_ACTIVE_HIGH>;
		intn-gpios = <&gpx2 3 GPIO_ACTIVE_HIGH>;
		reset-gpios = <&gpm2 4 GPIO_ACTIVE_LOW>;
		initial-mode = <1>;
	};
```

配置内核增加驱动，默认已包含，不需要修改

![image-20210923113528082](Linux%20Porting.assets/image-20210923113528082.png)



### 添加USB摄像头驱动

![image-20210923113839073](Linux%20Porting.assets/image-20210923113839073.png)

重新编译内核和设备树，系统启动后连接USB摄像头，在/dev目录下会生成video0设备文件。

## 文件系统移植

使用`fdisk -l`命令可以看到内核识别出的板载eMMC。

```bash
Disk /dev/mmcblk1: 3909 MB, 3909091328 bytes
226 heads, 33 sectors/track, 1023 cylinders
Units = cylinders of 7458 * 512 = 3818496 bytes

        Device Boot      Start         End      Blocks  Id System
/dev/mmcblk1p1             455        1020     2110614   c Win95 FAT32 (LBA)
/dev/mmcblk1p2               7          89      309507  83 Linux
/dev/mmcblk1p3              90         371     1051578  83 Linux
/dev/mmcblk1p4             372         454      309507  83 Linux

Partition table entries are not in disk order

```

1. 使用fdisk对eMMC重新分区，跳过头部的20M，分区大小1G。

```bash
[root@farsight ]# fdisk /dev/mmcblk1

Command (m for help): p

Disk /dev/mmcblk1: 3909 MB, 3909091328 bytes
226 heads, 33 sectors/track, 1023 cylinders
Units = cylinders of 7458 * 512 = 3818496 bytes

        Device Boot      Start         End      Blocks  Id System
/dev/mmcblk1p1             455        1020     2110614   c Win95 FAT32 (LBA)
/dev/mmcblk1p2               7          89      309507  83 Linux
/dev/mmcblk1p3              90         371     1051578  83 Linux
/dev/mmcblk1p4             372         454      309507  83 Linux

Partition table entries are not in disk order

Command (m for help): d
Partition number (1-4): 1

Command (m for help): d
Partition number (1-4): 2

Command (m for help): d
Partition number (1-4): 3

Command (m for help): d
Selected partition 4

Command (m for help): p

Disk /dev/mmcblk1: 3909 MB, 3909091328 bytes
226 heads, 33 sectors/track, 1023 cylinders
Units = cylinders of 7458 * 512 = 3818496 bytes

        Device Boot      Start         End      Blocks  Id System

Command (m for help): n
Command action
   e   extended
   p   primary partition (1-4)
p
Partition number (1-4): 1
First cylinder (1-119296, default 1): 640
Last cylinder or +size or +sizeM or +sizeK (640-119296, default 119296): 33408

Command (m for help): p

Disk /dev/mmcblk1: 3909 MB, 3909091328 bytes
4 heads, 16 sectors/track, 119296 cylinders
Units = cylinders of 64 * 512 = 32768 bytes

        Device Boot      Start         End      Blocks  Id System
/dev/mmcblk1p1             640       33408     1048608  83 Linux

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table

```

2. 格式化分区2

```bash
[root@farsight ]# mkfs.ext2 /dev/mmcblk1p1
warning: 8 blocks unused

Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
65664 inodes, 262144 blocks
13107 blocks (5%) reserved for the super user
First data block=0
Maximum filesystem blocks=4194304
8 block groups
32768 blocks per group, 32768 fragments per group
8208 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

```

3. 在虚拟机上创建根文件系统。

```bash
# 释放磁盘空间
sudo apt autoremove binutils gdb
# 禁用IPv6
sudo sh -c 'echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6'
# 安装模拟器和安装脚本
sudo apt install qemu-user-static debootstrap
# 创建空目录
sudo mkdir rootfs
# 下载并安装文件系统到rootfs目录
sudo debootstrap --arch armel --include openssh-server,sudo,libjpeg62-turbo stable ~/rootfs http://mirrors.ustc.edu.cn/debian
...
I: Base system installed successfully.
# 安装NFS服务器
sudo apt install nfs-kernel-server
# 配置NFS服务器
sudo sh -c 'echo "/home/student/rootfs *(rw,sync,no_subtree_check,no_root_squash)" >> /etc/exports'
# 重启NFS服务
sudo service nfs-kernel-server restart
# 挂载文件系统
mount -t nfs -o nolock 172.16.1.1:/home/student/rootfs /mnt
cd /mnt
# 如果/mnt目录下有文件说明挂载成功

```

4. 重启开发板，在Uboot中配置内核启动参数，验证根文件系统。

```bash
# 设置内核启动参数
setenv bootargs "root=/dev/nfs nfsroot=172.16.1.1:/home/student/rootfs,nolock,nfsvers=3 rw console=ttySAC2,115200 ip=172.16.1.2 clk_ignore_unused"
# 保存环境变量
saveenv
# 从NFS root启动
tftpboot 41000000 172.16.1.1:uImage
tftpboot 42000000 172.16.1.1:exynos4412-fs4412.dtb
bootm 41000000 - 42000000
# 系统启动后可以看到Login登录提示符表示文件系统制作成功
```

5. 配置并打包根文件系统。

```bash
# 修改root密码
sudo chroot ~/rootfs
root@debian:/# passwd
New password:
Retype new password:
passwd: password updated successfully
# 增加普通用户
useradd -m -g users -G sudo -s /bin/bash student
passwd student
New password:
Retype new password:
passwd: password updated successfully

exit
# 打包根文件系统
cd ~/rootfs
sudo tar cf ../rootfs.tar *
# 将打包好的文件系统移动到rootfs中
sudo mv rootfs.tar rootfs
```

6. 在开发板上烧写根文件系统

```bash
# 挂载eMMC
mount /dev/mmcblk1p1 /mnt
# 烧写文件系统到eMMC
tar xf /rootfs.tar -C /mnt
# 卸载eMMC文件系统
umount /mnt
```

7. 修改UBoot中的内核启动参数并保存

```bash
setenv bootargs "console=ttySAC2,115200 root=/dev/mmcblk1p1 rw rootwait clk_ignore_unused"
saveenv
```

8. 在Uboot中将内核及设备树烧写到eMMC

```bash
# 加载内核和设备树到内存
tftpboot 41000000 172.16.1.1:uImage
tftpboot 42000000 172.16.1.1:exynos4412-fs4412.dtb

# 将内存中的内核和设备树写入到eMMC，内核镜像大小不能超过4M（可以裁剪并更换压缩算法）
movi write kernel 41000000
movi write dtb 42000000

# 修改启动命令
setenv bootcmd "movi read kernel 41000000;movi read dtb 42000000;bootm 41000000 - 42000000"
saveenv
```

9. 重启开发板，Linux系统会自动从eMMC启动。

