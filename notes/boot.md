## FS4412启动流程

1. 执行iRom中的代码（出厂时固化在芯片中的代码）
2. 读取拨码开关选择启动设备
3. 加载BL1（初始化片上外设）
4. 加载BL2（初始化板级外设）== U-Boot
5. 加载Linux内核

## Uboot中的环境变量

* baudrate 配置串口波特率
* bootargs 传递给Linux内核的启动参数
* bootdelay 自动启动前的等待时间（秒）
* ipaddr 本机IP地址
* serverip TFTP服务器的IP地址

## 使用以太网连接虚拟机与开发板

1. 使用网线直连PC与开发板
2. 将虚拟机的网卡桥接到有线网卡
3. 配置虚拟机中的IP地址

```bash
ip address add 172.16.1.1/16 dev enp0s3
```

4. 在开发板的U-boot中配置IP（注意：需要与虚拟机在同一网段）

```bash
setenv ipaddr 172.16.1.2
```

5. 从开发板ping虚拟机IP地址

```bash
ping 172.16.1.1
```

6. 在虚拟机上安装TFTP服务器

```bash
sudo apt install tftpd-hpa
```

7. 将需要通过TFTP下载的文件放到虚拟机/srv/tftp目录下

8. 在uboot中通过TFTP下载文件到内存地址（0x40000000地址开始是DRAM空间）

```bash
# 加载内核镜像到41000000地址
tftpboot 41000000 172.16.1.1:uImage
# 加载设备树到42000000地址
tftpboot 42000000 172.16.1.1:exynos4412-fs4412.dtb
# 加载内存文件系统到43000000地址
tftpboot 43000000 172.16.1.1:ramdisk.img
```

9. 使用bootm命令启动Linux系统

```bash
bootm 内核加载地址 文件系统加载地址 设备树加载地址
```

## Linux 内核移植

内核移植的主要工作：配置Linux内核，修改设备树

1. 搭建内核编译环境

```bash
# 安装交叉编译工具链
sudo apt install gcc-arm-linux-gnueabi
# 安装debian的Linux内核源码
sudo apt install linux-source
# 安装依赖软件包
sudo apt install libssl-dev u-boot-tools
```

2. 将Linux内核源码移动到用户目录下并解压

```bash
sudo mv /usr/srv/linux-source-5.10.tar.xz ~
tar axf linux-source-5.10.tar.xz
```

3. 配置内核

```bash
cd linux-source-5.10
make exynos_defconfig ARCH=arm
```

4. 编译内核

```bash
make uImage ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- LOADADDR=0x40008000
# 编译好的内核镜像文件放在arch/arm/boot/uImage
```

5. 修改设备树

```diff
--- D:\Desktop\exynos4412-fs4412.dts	2021-09-22 16:34:49.000000000 +0800
+++ D:\Desktop\linux-source-5.10\arch\arm\boot\dts\exynos4412-origen.dts	2021-06-23 20:42:55.000000000 +0800
@@ -14,14 +14,14 @@
 #include <dt-bindings/clock/samsung,s2mps11.h>
 #include <dt-bindings/gpio/gpio.h>
 #include <dt-bindings/input/input.h>
 #include "exynos-mfc-reserved-memory.dtsi"
 
 / {
-	model = "FarSight FS4412 evaluation board based on Exynos4412";
-	compatible = "farsight,fs4412", "samsung,exynos4412", "samsung,exynos4";
+	model = "Insignal Origen evaluation board based on Exynos4412";
+	compatible = "insignal,origen4412", "samsung,exynos4412", "samsung,exynos4";
 
 	memory@40000000 {
 		device_type = "memory";
 		reg = <0x40000000 0x40000000>;
 	};
 
@@ -104,30 +104,30 @@
 &fimd {
 	pinctrl-0 = <&lcd_clk &lcd_data24 &pwm1_out>;
 	pinctrl-names = "default";
 	status = "okay";
 };
 
-&i2c_1 {
+&i2c_0 {
 	#address-cells = <1>;
 	#size-cells = <0>;
 	samsung,i2c-sda-delay = <100>;
 	samsung,i2c-max-bus-freq = <20000>;
-	pinctrl-0 = <&i2c1_bus>;
+	pinctrl-0 = <&i2c0_bus>;
 	pinctrl-names = "default";
 	status = "okay";
 
 	s5m8767_pmic@66 {
 		compatible = "samsung,s5m8767-pmic";
 		reg = <0x66>;
 
 		s5m8767,pmic-buck-default-dvs-idx = <3>;
 
-		s5m8767,pmic-buck-dvs-gpios = <&gpb 5 GPIO_ACTIVE_HIGH>,
-						 <&gpb 6 GPIO_ACTIVE_HIGH>,
-						 <&gpb 7 GPIO_ACTIVE_HIGH>;
+		s5m8767,pmic-buck-dvs-gpios = <&gpx2 3 GPIO_ACTIVE_HIGH>,
+						 <&gpx2 4 GPIO_ACTIVE_HIGH>,
+						 <&gpx2 5 GPIO_ACTIVE_HIGH>;
 
 		s5m8767,pmic-buck-ds-gpios = <&gpm3 5 GPIO_ACTIVE_HIGH>,
 						<&gpm3 6 GPIO_ACTIVE_HIGH>,
 						<&gpm3 7 GPIO_ACTIVE_HIGH>;
 
 		s5m8767,pmic-buck2-dvs-voltage = <1250000>, <1200000>,

```

6. 修改Makefile，编译设备树

![image-20210922164918814](boot.assets/image-20210922164918814.png)

```bash
make dtbs ARCH=arm
#编译好的设备树文件放在arch/arm/boot/dts/exynos4412-fs4412.dtb
```

