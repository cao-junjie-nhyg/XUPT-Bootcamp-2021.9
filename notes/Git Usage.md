## Git安装

1. Linux

```bash
sudo apt install git
```

2. Windows

```bash
# 国内下载镜像
https://mirrors.tuna.tsinghua.edu.cn/github-release/git-for-windows/git/LatestRelease/
```

## Git配置

1. 配置用户名（代码的作者）

```bash
git config --global user.name "LIU Yu"
```

2. 配置Email（作者的联系方式）

```bash
git config --global user.email liuy_xa@hqyj.com
```

3. 配置文本编辑器（编写提交信息）

```bash
git config --global core.editor C:\Windows\System32\notepad.exe
```

4. 查看修改的配置

```bash
git config --list
```

## 保存修改

1. 创建工作目录和本地代码库

```bash
git init
```

2. 查看当前工作目录状态

```bash
git status
```

3. 将修改添加到暂存区

```bash
git add main.c CMakeLists.txt
```

4. 将修改提交到代码库

```bash
git commit
# 必须在弹出的文本编辑器中填写提交信息
```

## 比较差异

1. 查看提交历史

```bash
git log
```

2. 比较差异

```bash
git diff <修改前的提交ID> <修改后的提交ID>

#提交ID可以使用HEAD~数字代替
```

## 恢复代码

```bash
git checkout HEAD .
# HEAD表示恢复到代码库中的最新版本
# 句点代表所有文件
```

## 删除和重命名文件

```bash
git rm 文件名
git mv 旧文件名 新文件名
git commit
```

## 下载远程库

```bash
git clone https://gitee.com/hqyjxa/XUPT-Bootcamp-2021.9.git
```

## 提交PR

1. 复制远程库

![image-20210914162810543](Git%20Usage.assets/image-20210914162810543.png)

2. 将复制的远程库下载到本地

```bash
git clone https://gitee.com/<自己的账号路径>/XUPT-Bootcamp-2021.9.git
```

3. 在homework目录下增加以自己名字拼音和学号命名的目录，并将自己的照片放到此目录下。

4. 将新增的目录添加到暂存区，并提交到本地代码库，提交信息中写清是哪一天的作业。

```bash
git add homework/<自己命令的目录>
git commit
```

5. 将本地代码库的修改同步到复制的远程库中

```bash
git push
# 需要注册时填写的邮箱或手机号和注册使用的密码
```

6. 创建拉取请求

![image-20210914165140859](Git%20Usage.assets/image-20210914165140859.png)

![image-20210914165636200](Git%20Usage.assets/image-20210914165636200.png)
