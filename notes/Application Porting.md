## 应用程序移植

1. 生成交叉编译makefile

```bash
CC=arm-linux-gnueabi-gcc cmake .
make
```

2. 将摄像头采集的YUV422格式的图像编码为JPEG格式

```C
//图像宽度
int width = 640;
//图像高度
int height = 480;
//压缩质量
int quality = 80;

//将YUV422压缩为JPEG
int jpeg_encode(const char *src, size_t src_size, unsigned char **dst, size_t *dst_size, int quality)
{
    //JPEG compression object
    struct jpeg_compress_struct cinfo;
    //JPEG error handler
    struct jpeg_error_mgr jerr;
    //set up the error handler
    cinfo.err = jpeg_std_error(&jerr);
    //initialize the JPEG compression object
    jpeg_create_compress(&cinfo);
    //将编码之后的图像写入内存(自动分配内存空间)
    jpeg_mem_dest(&cinfo, dst, dst_size);
    //压缩图像大小
    cinfo.image_width = width;
    cinfo.image_height = height;
    //每像素通道数
    cinfo.input_components = 3;
    //使用YCbCr颜色空间
    cinfo.in_color_space = JCS_YCbCr;
    //设置默认压缩参数
    jpeg_set_defaults(&cinfo);
    //设置压缩质量
    jpeg_set_quality(&cinfo, quality, TRUE);
    //开始压缩
    jpeg_start_compress(&cinfo, TRUE);

    JSAMPROW jrow;
    unsigned char buf[width * 3];
    while (cinfo.next_scanline < cinfo.image_height)
    {
        //将每个像素由YUV422转为YUV444
        for (int i = 0; i < cinfo.image_width; i += 2)
        {
            //Y0U0 Y1V1 Y2U2 Y3V3
            buf[i * 3] = src[i * 2];         //Y0 = Y0
            buf[i * 3 + 1] = src[i * 2 + 1]; //U0 = U0
            buf[i * 3 + 2] = src[i * 2 + 3]; //V0 = V1
            buf[i * 3 + 3] = src[i * 2 + 2]; //Y1 = Y1
            buf[i * 3 + 4] = src[i * 2 + 1]; //U1 = U0
            buf[i * 3 + 5] = src[i * 2 + 3]; //V1 = V1
        }
        jrow = (JSAMPROW)&buf;
        jpeg_write_scanlines(&cinfo, &jrow, 1);
        src += width * 2;
    }
    //停止压缩
    jpeg_finish_compress(&cinfo);
    //释放内存
    jpeg_destroy_compress(&cinfo);

    return 0;
}

//摄像头采集线程处理函数
void* capture_thread(void* data)
{
    int fd = *(int*)data;

    cam_start(fd);

    while (1) //循环处理摄像头捕获到的每一帧图像
    {
        //出队（阻塞）
        struct v4l2_buffer* buffer = cam_dequeue(fd);
        if (buffer == NULL)
        {
            continue;
        }
        //设置当前帧
        pthread_mutex_lock(&current_frame.lock);
        if (current_frame.size)
        {
            free(current_frame.addr);
            current_frame.size = 0;
        }
        jpeg_encode(buffers[buffer->index], buffer->bytesused, &current_frame.addr, &current_frame.size, quality);
        pthread_mutex_unlock(&current_frame.lock);

        //重新将缓冲区入队
        cam_queue(fd, buffer);
    }

    cam_stop(fd);

    return NULL;
}
```

3. 在虚拟机上安装libjpeg开发库

```bash
sudo dpkg --add-architecture armel
apt download libjpeg62-turbo-dev:armel
sudo dpkg -i --force-all libjpeg62-turbo-dev*_armel.deb
```

4. 在开发板上安装libjpeg库

```bash
sudo apt install libjpeg62-turbo
```

5. 修改CMakeList，链接libjpeg库

```cmake
target_link_libraries(server pthread jpeg)
```

6. 编译服务器程序，上传到开发板运行